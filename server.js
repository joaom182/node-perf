const cluster = require('cluster');
const Path = require('path');
const Hapi = require('hapi');
const Inert = require('inert');
const port = process.env.port || 1337;

const server = new Hapi.Server({
    connections: {
        routes: {
            files: {
                relativeTo: Path.join(__dirname, 'public')
            }
        }
    }
});
server.connection({ port: port });
server.register(Inert, () => {});

server.route({
    method: 'GET',
    path: '/numeros-primos',
    handler: require('./primeNumberHandler')
});

server.route({
    method: 'GET',
    path: '/video',
    handler: require('./readFileMultipleTimes')
});

server.route({
    method: 'GET',
    path: '/hello',
    handler: function(request, reply){
        reply('Hello Word');
    }
});

server.start((err) => {
    if (err)
        throw err;

    console.log('Server running at:', server.info.uri);
});
