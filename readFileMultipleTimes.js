var fs = require('fs');

function _readFileMultipleTimes(request, reply){
    var thread = 1;
    var count = 1;
    while(count <= 6) {
        fs.readFile('./public/video.mp4', function (err, data) {
            console.log(`Thread # ${thread++} finish`);
        });
        count++;
    }
    reply(`Video read ${count} times`);
}

module.exports = _readFileMultipleTimes;