var isPrime = require('./number-util').isPrime;
var count = 1;

function _primeNumberHandler(request, reply){
    var number = 0;
    var numberOfPrimes = 0;
    while(true) {
        if(isPrime(++number))
            numberOfPrimes++;

        if(numberOfPrimes === 1000000)
            break;
    }
    reply(`Primes # ${count++} | Number: ${number} | Finished in: ${endRequest.toString()} ms`);

}

module.exports = _primeNumberHandler;